#ifndef __RANDOM_H__
#define __RANDOM_H__

#include <stdlib.h>
#include <time.h>

template<typename T> void _random(unsigned max, T& result){
    result = rand() % max;
}

long _getns(){
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_nsec;
}

void random(unsigned max, int& result){
    srand(_getns());
    _random(max, result);
}

int random(unsigned max){
    srand(_getns());
    int result;
    _random(max, result);
    return result;
}

void random(char& result){
    srand(_getns());
    _random(256, result);
}

char random(){
    srand(_getns());
    char result;
    _random(256, result);
    return result;
}


//return a random string of length characters,
//containing lowercase letters and numbers
void random(unsigned length, char * result){
    srand(_getns()); //initialize random seed
    for(unsigned i = 0; i < length; i++)
    {
        int c; _random(36, c);
        if(c < 10)
            c += '0';
        else
            c += 'a' - 10;
        result[i] = c;
    }
    result[length] = '\0';
}

#endif